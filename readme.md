# Upstart

Upstart is a very simple starting point for getting projects up and running.

## Quick start

Clone this repo:


````
mkdir MyProject
cd MyProject
git clone --depth=1 --branch=master git@github.com:deanacus/upstart.git .
rm -rf .git
````

Install dependencies:

	npm install

Initialise the project:

	gulp init

Get to work. Make changes in `src`, deploy `build`. Run `gulp watch` to live reload your shit.

## Details

### HTML

Super basic HTML file.

### Sass

Minimal starting point:

	scss/
	├── _reset.scss			// Basic reset
	├── _defaults.scss		// Standard values
	├── _grid.scss			// My simple grid system
	├── _mixins.scss			// Standard mixins
	└── style.scss			// all @imports

### Javascript

Nothing special, just a wrapper funtion to contain all the scripts, and an event to call that wrapper function when the page is loaded, plus a separate file for some standard event functions I've written in VanillaJS
