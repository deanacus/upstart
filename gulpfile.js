var gulp = require('gulp'),

    sass = require('gulp-sass'),
    cssmin = require('gulp-cssmin'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    jsmin = require('gulp-jsmin'),
    del = require('del'),
    browserSync = require('browser-sync').create(),

    input  = {
      'html': 'src/**/*.html',
      'css': 'src/assets/scss/**/*.scss',
      'js': 'src/assets/js/**/*.js',
      'img': 'src/assets/img/**/*',
      'fonts': 'src/assets/fonts/**/*'
    },

    output = {
      'html': 'build/',
      'css': 'build/assets/css/',
      'js': 'build/assets/js/',
      'img': 'build/assets/img/',
      'fonts': 'build/assets/fonts/'
    };

// Set up BrowserSync
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'build'
    },
  })
})

// Compile and minify SASS
gulp.task('sass', function() {
  gulp.src(input.css)
  .pipe(sourcemaps.init())
  .pipe(sass())
  .pipe(cssmin())
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest(output.css))
  .pipe(browserSync.reload({stream: true}));
});

// Compile and minify JS
gulp.task('js', function() {
  gulp.src(input.js)
  .pipe(concat('app.js'))
  .pipe(jsmin())
  .pipe(gulp.dest(output.js))
  .pipe(browserSync.reload({stream: true}));
});

gulp.task('img', function() {
  gulp.src(input.img)
  .pipe(gulp.dest(output.img))
  .pipe(browserSync.reload({stream: true}));
});

gulp.task('fonts', function() {
  gulp.src(input.fonts)
  .pipe(gulp.dest(output.fonts))
  .pipe(browserSync.reload({stream: true}));
});


// Copy HTML files to dist
gulp.task('html', function() {
  gulp.src(input.html)
  .pipe(gulp.dest(output.html))
  .pipe(browserSync.reload({stream: true}));
});

// Watch directories
gulp.task('watch',['browserSync'], function() {
  gulp.watch(input.html, ['html']);
  gulp.watch(input.css, ['sass']);
  gulp.watch(input.js, ['js']);
});

gulp.task('clean', function() {
  del.sync(['build/**']);
});

// Gulp Init
gulp.task('init', ['clean', 'sass', 'js', 'img', 'fonts', 'html']);

// Default task
gulp.task('default', ['watch', 'sass', 'js', 'img', 'fonts', 'html']);