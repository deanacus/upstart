// Reusable Utilities
 
// Easy element selctor
// 
// USAGE $(".target-element")
var $ = document.querySelectorAll.bind(document);
 
// Universal click event binding
// 
// USAGE: click($(".target-element"), targetFunction);
function click(el, func){
  for( var i=0; i < el.length; i++ ){
    el[i].addEventListener('click', func, false);
  }
}
 
// Universal change event binding
// 
// USAGE: change($(".target-element"), targetFunction);
function change(el, func){
  for( var i=0; i < el.length; i++ ){
    el[i].addEventListener('change', func, false);
  }
}

//Get URL parameter by key name
//
// USAGE: getQueryValue("key-name");

function getQueryValue(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if(pair[0] == variable){
      return decodeURIComponent(pair[1]);
    }
  }
  return(false);
}