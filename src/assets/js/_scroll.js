function smoothScroller(target, duration){

  /* Usage: 
    smoothScroller(".scroll", 500);
    Takes two parameters: the querySelectorAll selector, and the duration of the animation in milliseconds
  */

  var scrollLinks = document.querySelectorAll(target);
 
  // Add click handlers to each link in the scrollLinks nodeList
  [].forEach.call(scrollLinks, function(el) {
    el.addEventListener('click', function(e) {
      e.preventDefault();
      var targetID = el.getAttribute('href');
      var target = document.querySelector(targetID);
      scroll(target, duration);
    });
  })
 
  function scroll(target, duration){

    // get current window position
    var currentPosition = window.pageYOffset;
    // get position of target element
    var targetPosition = target.offsetTop;
    // get distance between current window position and target element postion (target position - window position)
    var distance = targetPosition - currentPosition;
    //determine size of the increments (distance / (duration / number of times to run scroll animation))
    var inc = distance / (duration/25);
    
    // Run the animation
    function animateScroll(inc){
      window.scrollBy(0,inc);
      stopAnimation(inc);
    }

    function stopAnimation(inc){
      // Work out if we've got further to travel
      var currentPos = window.pageYOffset;
      // If we're scrolling down
      if ( inc >= 0 ) {
        // Stop the animation if have reached our destintion OR the bottom of the page
        if ( currentPos >= targetPosition - inc || ((window.innerHeight + currentPos) >= document.body.offsetHeight) ) {
          clearInterval(runAnimation);
        };
      } 
      // If we're scrolling up
      else {
        // Stop the animation if we've reached our destination
        if ( currentPos <= (targetPosition - inc || 0) ) {
          clearInterval(runAnimation);
        };
      };
    };

    // Set up the looping interval ro run the animation
    var runAnimation = setInterval(animateScroll, duration/25, inc);
  }
}