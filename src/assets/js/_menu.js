var menu = document.getElementsByClassName("fullscreen-menu")[0];
var trigger = document.getElementsByClassName("fullscreen-menu-toggle")[0];

function toggleMenu() {
  if (menu.classList.contains('is-active')) {
    menu.classList.remove('is-active');
    trigger.classList.remove('is-active');
  } 
  else {
    menu.classList.add('is-active');
    trigger.classList.add('is-active');
  }
}

// Close menu if opened and esc key pressed
window.addEventListener("keyup", function(e) {
  if (menu.classList.contains('is-active') && e.keyCode == 27) {
    menu.classList.remove('is-active');
    trigger.classList.remove('is-active');
  };
}, false);

// Listen for trigger to be clicked
trigger.addEventListener('click', toggleMenu);
menu.addEventListener('click', toggleMenu);